package com.epam.synchronization;

public class ThreadThree extends Thread {
  MainSync t;

  ThreadThree(MainSync t) {
    this.t = t;
  }

  public void run() {
    synchronized (t) {
      t.printTable();
      t.printTable2();
      t.printTable3();
      try {
        Thread.sleep(300);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }
}
