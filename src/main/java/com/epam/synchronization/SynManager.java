package com.epam.synchronization;

public class SynManager {

  public void printSynchronize() {
    getOneThreadObj();
    getMultiplThreadObj();
  }

  private void getMultiplThreadObj() {
    MainSync mainSync = new MainSync();
    ThreadOne threadOne = new ThreadOne(mainSync);
    ThreadTwo threadTwo = new ThreadTwo(mainSync);
    ThreadThree threadThree = new ThreadThree(mainSync);
    threadOne.start();
    threadTwo.start();
    threadThree.start();
  }

  private void getOneThreadObj() {
    MainSync mainSync = new MainSync();
    ThreadOne threadOne = new ThreadOne(mainSync);
    ThreadOne threadTwo = new ThreadOne(mainSync);
    ThreadOne threadThree = new ThreadOne(mainSync);

    threadOne.start();
    threadTwo.start();
    threadThree.start();
  }
}
