package com.epam.synchronization;

public class ThreadTwo extends Thread {
  MainSync t;

  ThreadTwo(MainSync t) {
    this.t = t;
  }

  public void run() {
    synchronized (t) {
      t.printTable();
      t.printTable2();
      try {
        Thread.sleep(300);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      t.printTable3();
    }
  }
}
