package com.epam.synchronization;

public class ThreadOne extends Thread {
  MainSync t;

  ThreadOne(MainSync t) {
    this.t = t;
  }

  public void run() {
    synchronized (t) {
      t.printTable();
      try {
        Thread.sleep(300);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
      t.printTable2();
      t.printTable3();
    }
  }
}
