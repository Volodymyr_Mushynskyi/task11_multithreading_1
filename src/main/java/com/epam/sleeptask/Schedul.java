package com.epam.sleeptask;

import java.util.Random;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledFuture;
import java.util.concurrent.TimeUnit;

public class Schedul {

  public void printSchedulTask() throws InterruptedException {
    ScheduledExecutorService executor = Executors.newScheduledThreadPool(2);
    Runnable task = () -> System.out.println(" Task 1 ");
    Runnable tasktwo = () -> System.out.println(" Task 2 ");
    Random random = new Random();
    ScheduledFuture<?> future = executor.schedule(task, random.nextInt(9) + 1, TimeUnit.SECONDS);
    ScheduledFuture<?> future2 = executor.schedule(tasktwo, random.nextInt(9) + 1, TimeUnit.SECONDS);
    long remainingDelay = future.getDelay(TimeUnit.MILLISECONDS);
    System.out.println("Remaining Delay: " + remainingDelay);
    long remainingDelay2 = future2.getDelay(TimeUnit.MILLISECONDS);
    System.out.println("Remaining Delay: " + remainingDelay2);
  }
}
