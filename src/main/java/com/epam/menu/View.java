package com.epam.menu;

import com.epam.fibonachi.Sum;
import com.epam.fibonachi.Task;
import com.epam.sleeptask.Schedul;
import com.epam.synchronization.SynManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class View {
  private Map<String, String> menu;
  private Map<String, Printable> methodsMenu;
  private static Logger logger = LogManager.getLogger(View.class);
  private static Scanner input = new Scanner(System.in);

  public View() {
    menu = new LinkedHashMap<>();
    menu.put("1", "Press  1 - execute printFibonachi");
    menu.put("2", "Press  2 - execute printSleepTask");
    menu.put("3", "Press  3 - execute printPingPong");
    menu.put("4", "Press  4 - execute printSerealization");
    menu.put("0", "Press  0 - exit");

    methodsMenu = new LinkedHashMap<>();
    methodsMenu.put("1", this::printFibonachi);
    methodsMenu.put("2", this::printSleepTask);
    methodsMenu.put("3", this::printPingPong);
    methodsMenu.put("4", this::printSynchronization);
  }

  private void printPingPong() {
    Sum sum = new Sum(10);
    Future<Integer> future = Executors.newSingleThreadExecutor().submit(sum);
    try {
      System.out.println(future.get().intValue());
    } catch (InterruptedException e) {
      e.printStackTrace();
    } catch (ExecutionException e) {
      e.printStackTrace();
    }
  }

  private void printFibonachi() {
    new Task(5).createSequence();
  }

  private void printSynchronization() {
    new SynManager().printSynchronize();
  }

  private void printSleepTask() {
    try {
      new Schedul().printSchedulTask();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  private void printMenuAction() {
    System.out.println("--------------MENU-----------\n");
    for (String str : menu.values()) {
      System.out.println(str);
    }
  }

  public void showMenu() {
    String keyMenu;
    do {
      printMenuAction();
      keyMenu = input.nextLine().toUpperCase();
      try {
        methodsMenu.get(keyMenu).print();
      } catch (Exception e) {
      }
    } while (!keyMenu.equals("0"));
  }
}
