package com.epam.pingpong;

import java.util.concurrent.locks.Lock;

public class PingPong implements Runnable {
  private String word;
  private Lock lock;

  public PingPong(String word, Lock lock) {
    this.word = word;
    this.lock = lock;
  }

  @Override
  public void run() {
    System.out.println(word);
    lock.notifyAll();
    try {
      lock.wait();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }
}
