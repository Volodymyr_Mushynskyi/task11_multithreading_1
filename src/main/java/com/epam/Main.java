package com.epam;

import com.epam.menu.View;

import java.util.concurrent.ExecutionException;

public class Main {
  public static void main(String[] args) throws ExecutionException, InterruptedException {
    new View().showMenu();
  }
}
