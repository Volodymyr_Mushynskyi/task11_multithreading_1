package com.epam.fibonachi;

import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Task {
  private static int maxOddDigit;
  private static int maxEvenDigit;
  private int sizeofSet;

  public Task(int sizeofSet) {
    this.sizeofSet = sizeofSet;
  }

  public void createSequence() {
    int[] f = new int[sizeofSet + 2];
    f[0] = 0;
    f[1] = 1;
    System.out.print("Fibonacci sequence: ");
    for (int i = 2; i <= sizeofSet; i++) {
      f[i] = f[i - 1] + f[i - 2];
      if (Math.abs(f[i]) % 2 == 0) {
        maxEvenDigit = f[i];
      }
      if (Math.abs(f[i]) % 2 == 1) {
        maxOddDigit = f[i];
      }
      System.out.print(f[i] + " ");
    }
    System.out.println();
    new MaxEventhread(maxEvenDigit).start();
    new MaxOddthread(maxOddDigit).start();

    cachedExecutors();
    fixedExecutors();
    singlExecutors();
  }

  private void singlExecutors() {
    ExecutorService execService = Executors.newSingleThreadExecutor();
    execService.execute(new MaxEventhread(maxEvenDigit));
    execService.execute(new MaxOddthread(maxOddDigit));
    execService.shutdown();
  }

  private void fixedExecutors() {
    ExecutorService execService = Executors.newFixedThreadPool(2);
    execService.execute(new MaxEventhread(maxEvenDigit));
    execService.execute(new MaxOddthread(maxOddDigit));
    execService.shutdown();
  }

  private void cachedExecutors() {
    ExecutorService execService = Executors.newCachedThreadPool();
    execService.execute(new MaxEventhread(maxEvenDigit));
    execService.execute(new MaxOddthread(maxOddDigit));
    execService.shutdown();
  }
}
