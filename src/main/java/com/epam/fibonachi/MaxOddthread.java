package com.epam.fibonachi;

public class MaxOddthread extends Thread {
  private int min;

  public MaxOddthread(int min) {
    this.min = min;
  }

  @Override
  public void run() {
    System.out.println("Max Odd digit :" + min);
  }
}
