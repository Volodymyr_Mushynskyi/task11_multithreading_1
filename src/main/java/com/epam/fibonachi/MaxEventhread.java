package com.epam.fibonachi;

public class MaxEventhread extends Thread {

  private int max;

  public MaxEventhread(int max) {
    this.max = max;
  }

  @Override
  public void run() {
    System.out.println("Max Even digit :" + max);
  }
}
