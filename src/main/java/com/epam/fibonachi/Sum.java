package com.epam.fibonachi;

import java.util.concurrent.Callable;

public class Sum implements Callable<Integer> {

  private static int maxOddDigit;
  private static int maxEvenDigit;
  private int sizeofSet;
  private int sum;

  public Sum(int sizeofSet) {
    this.sizeofSet = sizeofSet;
  }

  @Override
  public Integer call() throws Exception {
    int[] f = new int[sizeofSet + 2];
    f[0] = 0;
    f[1] = 1;
    System.out.print("Fibonacci sum: ");
    for (int i = 2; i <= sizeofSet; i++) {
      f[i] = f[i - 1] + f[i - 2];
      if (Math.abs(f[i]) % 2 == 0) {
        maxEvenDigit = f[i];
        sum += f[i];
      }
      if (Math.abs(f[i]) % 2 == 1) {
        maxOddDigit = f[i];
        sum += f[i];
      }
    }
    return sum;
  }
}
